import math
import sys
import getopt

VARIABLES = 0
SLACK = 0
ARTIFICIALS = 0
CONSTRAINTS = 0

def only_one(tabloid, variable):
        non_z = 0
        non_z_idx = -1
        for j in range(len(tabloid)):
            if(tabloid[j][variable] != 0):
                non_z += 1
                non_z_idx = j
        if(non_z == 1):
            return non_z_idx
        else:
            return -1



def print_matrix(mat):
    to_print = ''
    for i in range(VARIABLES):
        to_print += '% 15s' % ('x'+str(i+1))
    for i in range(SLACK):
        to_print += '% 15s' % ('s'+str(i+1))
    for i in range(ARTIFICIALS):
        to_print += '% 15s' % ('A'+str(i+1))
    to_print += '% 15s' % 'RHS'
    print(to_print)

    for i in range(len(mat)):
        to_print = ''
        for j in range(len(mat[i])):
            to_print += printable(mat[i][j])
        print(to_print)

def printable(num):
    if(type(num) == type(1.5)):
        return '% 15s' % ('%4.4f' % num)
    else:
        if(num.imag == 0):
            return printable(float(num.real))
        if(num.real >0):
            sign = '+'
        else:
            sign = ''
        return '% 15s' % ('%3.3fM%s%3.3f' % (num.imag, sign, num.real))
        


#compares two numbers, with j = M
def is_less(a, b):
    if(type(a) == type(1j)):
        if(type(b) == type(a)):
            return a.imag < b.imag
        else:
            if(a.imag < 0):
                return True
            if(a.imag > 0):
                return False
            else:
                return a.real < b

    elif(type(b) == type(1j)):
            if(b.imag < 0):
                return False
            if(b.imag > 0):
                return True
            else:
                return a < b.real

    else:
        return a < b


def do_simplex(mat):
    most_negative = 0
    most_negative_idx = -1

    variables = len(mat[0])-1
    for i in range(variables):
        if(is_less(mat[-1][i], most_negative)):
            most_negative = mat[-1][i]
            most_negative_idx = i
    while (most_negative_idx != -1):
        print_matrix(mat)
        smallest_ratio = math.inf
        smallest_ratio_idx = -1
        for i in range(CONSTRAINTS):
            if(mat[i][most_negative_idx] <= 0.0):
                continue
            ratio = mat[i][-1]/mat[i][most_negative_idx]
            if(ratio >= 0.0 and ratio < smallest_ratio):
                smallest_ratio = ratio
                smallest_ratio_idx = i

        #unbound
        if(smallest_ratio_idx == -1):
            print('Unbound solution')
            sys.exit(0)
             
        for row in range(len(mat)):
            if (row == smallest_ratio_idx):
                continue
            ratio = mat[row][most_negative_idx]/mat[smallest_ratio_idx][most_negative_idx]
            for column in range(variables+1):
                mat[row][column] -= mat[smallest_ratio_idx][column] * ratio

        ratio = mat[smallest_ratio_idx][most_negative_idx]
        for i in range(variables+1):
            mat[smallest_ratio_idx][i] = mat[smallest_ratio_idx][i]/ratio
        print('PIVOT: row %d, column %d' % (smallest_ratio_idx, most_negative_idx)) 

        most_negative = 0
        most_negative_idx = -1
        for i in range(variables):
            if(is_less(mat[-1][i], most_negative)):
                most_negative = mat[-1][i]
                most_negative_idx = i

    print_matrix(mat)

def main():
    global SLACK
    global VARIABLES
    global ARTIFICIALS
    global CONSTRAINTS

    opts, other = getopt.gnu_getopt(sys.argv[1:],  'ho:m:')
    
    method = 2
    #open input file to stdin
    sys.stdin = open(other[0], 'r')

    opts.sort()
    if(len(opts) > 0 and opts[0][0] == '-h'):
        help_file = open('helpfile', 'r')
        print(help_file.read())
        opts = opts[1:]
    if(len(opts) > 0 and opts[0][0] == '-m'):
        #set solving method
        method = int(opts[0][1])
        opts = opts[1:]
    if(len(opts) > 0):
        #open output file to stdout
        sys.stdout = open(opts[0][1], 'w')

    #multiply coefficients of objective function by -1 if doing maximization
    obj_type = input()
    if(obj_type == "min"):
        obj_type = 1
    else:
        obj_type = -1
    
    inp = input().split(',')
    VARIABLES = int(inp[0])
    constraints = int(inp[1])
    CONSTRAINTS = constraints

    objective = []
    inp = input().split(',')
    constraint_length = VARIABLES
    tabloid = []
    constraint_RHS = []
    #get obj coefficients
    for i in range(VARIABLES):
        objective.append(float(inp[i]) * obj_type)
    artificials = []
    #read constraints
    for i in range(constraints):
        inp = input().split(',')
        constraint = []
        constraint_RHS.append(float(inp[-2]))
        multiplier = 1
        #make RHS non-negative
        if(constraint_RHS[-1] < 0):
            multiplier = -1
            constraint_RHS[-1] = -constraint_RHS[-1]
            if(inp[-1] == '<='):
                inp[-1] = '>='
            if(inp[-1] == '>='):
                inp[-1] = '<='
        #input coefficients
        for j in range(VARIABLES):
            constraint.append(float(inp[j])*multiplier)
        #pad to correct length
        constraint += [0] * (constraint_length - VARIABLES)
        #add slack and artificia variables
        if(inp[-1] == '<='):
            constraint.append(1)
            constraint_length += 1
            SLACK += 1
        if(inp[-1] == '>='):
            constraint.append(-1)
            constraint_length += 1
            SLACK += 1
            if(constraint_RHS[-1] > 0): 
                artificials.append(i)
        if(method == 1 and inp[-1] == '='):
            artificials.append(i)

        tabloid.append(constraint)
    #pad all constraints to length
    for i in range(constraints):
        tabloid[i] += [0] * (constraint_length - len(tabloid[i]))

    #pad objective to length and add it
    objective += [0] * (constraint_length - len(objective))
    tabloid.append(objective)
    
    ARTIFICIALS = len(artificials)
    #add artificial variables
    for i in artificials:
        tabloid[i] += [0] * (constraint_length - len(tabloid[i]))
        constraint_length += 1
        tabloid[i].append(1)
    #match rows to size
    for i in range(len(tabloid)):
        tabloid[i] += [0] * (constraint_length - len(tabloid[i]))

    #create phase 1 objective function
    if(method == 2):
        artificials_obj = []
        for i in range(VARIABLES + SLACK):
            t = 0
            for j in artificials:
                t += tabloid[j][i]
            artificials_obj.append(-t)
        artificials_obj += [0] * (constraint_length - len(artificials_obj))
        tabloid.append(artificials_obj)

    #pad everything to length
    for i in range(constraints):
        tabloid[i].append(constraint_RHS[i])
    for i in range(constraints, len(tabloid)):
        tabloid[i].append(0)

    if(method == 1):
        #remove M from artificials
        for i in artificials:
            for j in range(VARIABLES + SLACK):
                tabloid[-1][j] -= tabloid[i][j] * 1j
            tabloid[-1][-1] -= tabloid[i][-1] * 1j
        do_simplex(tabloid)

    if(method == 2):
        artificials_RHS = 0
        for i in artificials:
            artificials_RHS += constraint_RHS[i]
        tabloid[-1][-1] = artificials_RHS
        print("********PHASE 1*********")
        do_simplex(tabloid)
        print('')
        print('')
        print("********PHASE 2*********")
        tabloid = tabloid[:-1]
        for i in range(len(tabloid)):
            tabloid[i] = tabloid[i][:VARIABLES+SLACK] + tabloid[i][-1:]
        ARTIFICIALS = 0
        do_simplex(tabloid)
        

    print('')
    print('')
    print('')
    print('Best variable allocation:')
    for i in range(VARIABLES):
        idx = only_one(tabloid, i)
        value = 0
        if(idx != -1):
            is_alone = True
            for j in range(i):
                if(only_one(tabloid, j) == idx):
                    is_alone = False
            if(is_alone):
                value = tabloid[idx][-1];
        print('x%d = %f' % (i+1, value))
    if(method == 1):
        print('Best value of objective function: %f' % tabloid[-1][-1].real)
    else:
        print('Best value of objective function: %f' % tabloid[-1][-1])
    for i in range(len(tabloid)-1):
        if(tabloid[-1][i] == 0 and not only_one(tabloid,i)):
            print('Other optimal solutions are possible by pivoting on %d' % i)
    if(sys.stdout != __sys.stdout__):
        sys.stdout = sys.__stdout__
        for i in range(VARIABLES):
            idx = only_one(tabloid, i)
            value = 0
            if(idx != -1):
                is_alone = True
                for j in range(i):
                    if(only_one(tabloid, j) == idx):
                        is_alone = False
                if(is_alone):
                    value = tabloid[idx][-1];
            print('x%d = %f' % (i+1, value))
        if(method == 1):
            print('Best value of objective function: %f' % tabloid[-1][-1].real)
        else:
            print('Best value of objective function: %f' % tabloid[-1][-1])
        for i in range(len(tabloid)-1):
            if(tabloid[-1][i] == 0 and not only_one(tabloid,i)):
                print('Other optimal solutions are possible by pivoting on %d' % i)

main()
